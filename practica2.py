
class Empleado:
    def __init__(self, n, s):
        self.nombre = n
        self.nomina = s

    def calculadatos(self):
        return self.nomina*0.3

    def __str__(self):
        return "El empleado {name} debe pagar {tax:.2f}".format(name=self.nombre,tax=self.calculadatos())


EmpleadoPepe = Empleado("Pepe", 20000)
EmpleadoAna = Empleado("Ana", 30000)

total = EmpleadoPepe.calculadatos() + EmpleadoAna.calculadatos()

print(EmpleadoPepe)
print(EmpleadoAna)
